import os
from distutils.command.build import build

from django.core import management
from setuptools import find_packages, setup

from pretix_c_fslmude import __version__


try:
    with open(os.path.join(os.path.dirname(__file__), 'README.rst'), encoding='utf-8') as f:
        long_description = f.read()
except:
    long_description = ''


class CustomBuild(build):
    def run(self):
        management.call_command('compilemessages', verbosity=1)
        build.run(self)


cmdclass = {
    'build': CustomBuild
}


setup(
    name='pretix-c-fslmude',
    version=__version__,
    description='Customizations for tickets.fs.lmu.de',
    long_description=long_description,
    url='https://git.fs.lmu.de/GAF/pretix-c-fslmude',
    author='Martin Gross',
    author_email='martin@fs.lmu.de',
    license='Apache',

    install_requires=[],
    packages=find_packages(exclude=['tests', 'tests.*']),
    include_package_data=True,
    cmdclass=cmdclass,
    entry_points="""
[pretix.plugin]
pretix_c_fslmude=pretix_c_fslmude:PretixPluginMeta
""",
)
