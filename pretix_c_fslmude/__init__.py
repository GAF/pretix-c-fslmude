from django.utils.translation import gettext_lazy

try:
    from pretix.base.plugins import PluginConfig
except ImportError:
    raise RuntimeError("Please use pretix 2.7 or above to run this plugin!")

__version__ = '1.0.0'


class PluginApp(PluginConfig):
    name = 'pretix_c_fslmude'
    verbose_name = 'pretix Customizations for tickets.fs.lmu.de'

    class PretixPluginMeta:
        name = gettext_lazy('pretix Customizations for tickets.fs.lmu.de')
        author = 'Martin Gross'
        description = gettext_lazy('Customizations for tickets.fs.lmu.de')
        visible = False
        version = __version__
        category = 'CUSTOMIZATION'
        compatibility = "pretix>=2.7.0"

    def ready(self):
        from . import signals  # NOQA


default_app_config = 'pretix_c_fslmude.PluginApp'
